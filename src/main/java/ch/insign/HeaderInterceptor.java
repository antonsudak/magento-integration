package ch.insign;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class HeaderInterceptor extends AbstractPhaseInterceptor<Message> {

    public HeaderInterceptor() {
        super(Phase.PREPARE_SEND);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        Map<String, List> headers = (Map<String, List>) message.get(Message.PROTOCOL_HEADERS);

        try {
            headers.put("Authorization", Collections.singletonList("Bearer ouba1j40ol7cdbrmmcfw5bgac10k2h77"));
        } catch (Exception e) {
            throw new Fault(e);
        }
    }

    @Override
    public void handleFault(Message message) {

    }
}
