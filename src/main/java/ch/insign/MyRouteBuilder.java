package ch.insign;

import ch.insign.docker._9010.soap.default_services_salesorderrepositoryv1.*;
import com.futura4retail.ws.futura4ecs._2016._01.ECommerceServiceSoap;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageContentsList;

import javax.xml.namespace.QName;
import javax.xml.ws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

public class MyRouteBuilder extends RouteBuilder {

    public void configure() {
        from("timer:simple?period=5000")
                .setBody(constant(orderRequest()))
                .to(createMagentoEndpoint())
                .process(exchange -> {
                    MessageContentsList contentsList = (MessageContentsList) exchange.getIn().getBody();
                    SalesOrderRepositoryV1GetListResponse response = (SalesOrderRepositoryV1GetListResponse) contentsList.get(0);

                    exchange.getIn().setBody(response.getResult().getItems().getItem());
                })
                .split(body())
                .process(exchange -> {
                    SalesDataOrderInterface order = (SalesDataOrderInterface) exchange.getIn().getBody();
                    exchange.getOut().setBody(SendOrderRequestBuilder.fromMagento(order));
                })
                .to(createECommerceEndpoint());
    }


    private CxfEndpoint createMagentoEndpoint() {
        List<Interceptor<? extends Message>> outInterceptors = new ArrayList<>();
        outInterceptors.add(new HeaderInterceptor());

        CxfEndpoint endpoint = new CxfEndpoint();
        endpoint.getFeatures().add(getLoggingFeature());
        endpoint.setAddress("http://docker.insign.ch:9010/soap/default?services=salesOrderRepositoryV1");
        endpoint.setWsdlURL(getClass().getClassLoader().getResource("wsdl/salesOrderRepositoryV1.wsdl").getFile());
        endpoint.setEndpointName(new QName("http://docker.insign.ch:9010/soap/default?services=salesOrderRepositoryV1","salesOrderRepositoryV1Port"));
        endpoint.setServiceName(new QName("http://docker.insign.ch:9010/soap/default?services=salesOrderRepositoryV1","salesOrderRepositoryV1Service"));
        endpoint.setDefaultOperationNamespace("http://docker.insign.ch:9010/soap/default?services=salesOrderRepositoryV1");
        endpoint.setDefaultOperationName("salesOrderRepositoryV1GetList");
        endpoint.setOutInterceptors(outInterceptors);
        endpoint.setBindingId(SOAPBinding.SOAP12HTTP_BINDING);
        endpoint.setServiceClass(SalesOrderRepositoryV1PortType.class);
        endpoint.setCamelContext(getContext());

        return endpoint;
    }

    private SalesOrderRepositoryV1GetListRequest orderRequest() {
        SalesOrderRepositoryV1GetListRequest request = new SalesOrderRepositoryV1GetListRequest();
        FrameworkSearchCriteria searchCriteria = new FrameworkSearchCriteria();
        searchCriteria.setCurrentPage(2);
        searchCriteria.setPageSize(1);

        request.setSearchCriteria(searchCriteria);

        return request;
    }

    private CxfEndpoint createECommerceEndpoint() {
        CxfEndpoint endpoint = new CxfEndpoint();
        endpoint.getFeatures().add(getLoggingFeature());
        endpoint.setAddress("http://localhost/Futura4ECS/ECommerceService.asmx"); //TODO
        endpoint.setWsdlURL(getClass().getClassLoader().getResource("wsdl/ECommerceService.wsdl").getFile());
        endpoint.setEndpointName(new QName("http://ws.futura4retail.com/Futura4ECS/2016/01","ECommerceServiceSoap"));
        endpoint.setServiceName(new QName("http://ws.futura4retail.com/Futura4ECS/2016/01","ECommerceService"));
        endpoint.setBindingId(SOAPBinding.SOAP12HTTP_BINDING);
        endpoint.setDefaultOperationNamespace("http://ws.futura4retail.com/Futura4ECS/2016/01");
        endpoint.setDefaultOperationName("SendOrders");
        endpoint.setServiceClass(ECommerceServiceSoap.class);
        endpoint.setCamelContext(getContext());

        return endpoint;
    }

    private LoggingFeature getLoggingFeature() {
        LoggingFeature logging = new LoggingFeature();
        logging.setPrettyLogging(true);

        return logging;
    }
}
