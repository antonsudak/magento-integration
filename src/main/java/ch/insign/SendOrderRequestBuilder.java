package ch.insign;

import ch.insign.docker._9010.soap.default_services_catalogproductattributerepositoryv1.*;
import ch.insign.docker._9010.soap.default_services_catalogproductrepositoryv1.*;
import ch.insign.docker._9010.soap.default_services_catalogproductrepositoryv1.FrameworkAttributeInterface;
import ch.insign.docker._9010.soap.default_services_salesorderrepositoryv1.SalesDataOrderInterface;
import ch.insign.docker._9010.soap.default_services_salesorderrepositoryv1.SalesDataOrderItemInterface;
import com.futura4retail.ws.futura4ecs._2016._01.ArrayOfOrderPosition;
import com.futura4retail.ws.futura4ecs._2016._01.OrderPosition;
import com.futura4retail.ws.futura4ecs._2016._01.SendOrderRequest;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;

import javax.xml.ws.Service;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendOrderRequestBuilder {

    public static SendOrderRequest fromMagento(SalesDataOrderInterface order) {
        SendOrderRequest request = new SendOrderRequest();

        request.setCustomerId(order.getExtCustomerId());
        ArrayOfOrderPosition arrayOfOrderPosition = new ArrayOfOrderPosition();
        request.setOrderId(order.getExtOrderId());
        request.setCustomerId(order.getExtCustomerId());
        request.setOrderPositions(arrayOfOrderPosition);

        for (SalesDataOrderItemInterface orderItem : order.getItems().getItem()) {
            OrderPosition orderPosition = new OrderPosition();
            orderPosition.setArticleId(orderItem.getName());
            orderPosition.setQuantity(new BigDecimal(orderItem.getQtyOrdered()));
            orderPosition.setPrice(new BigDecimal(orderItem.getPrice()));
            orderPosition.setIsPercentDiscount(orderItem.getDiscountPercent() != null);
            orderPosition.setDiscount(orderPosition.isIsPercentDiscount() ?
                    new BigDecimal(orderItem.getDiscountPercent()) :
                    new BigDecimal(orderItem.getDiscountAmount()));

            CatalogDataProductInterface product = getProduct(orderItem.getSku());

            if (product != null)
                for (FrameworkAttributeInterface attribute: product.getCustomAttributes().getItem()) {
                    CatalogDataProductAttributeInterface attributeInfo = getAttribute(attribute.getAttributeCode());
                    List<EavDataAttributeOptionInterface> options = attributeInfo.getOptions().getItem();

                    String value = null;
                    for (EavDataAttributeOptionInterface option : options)
                        if (option.getValue().equals(((ElementNSImpl) attribute.getValue()).getTextContent()))
                            value = option.getLabel();

                    switch(attribute.getAttributeCode()) {
                        case "color":
                            orderPosition.setColor(value);
                            break;
                        case "size":
                            orderPosition.setSize(value);
                        default:
                            continue;
                    }
                }

            arrayOfOrderPosition.getOrderPosition().add(orderPosition);
        }

        return request;
    }

    private static CatalogDataProductInterface getProduct(String sku) {
        CatalogDataProductInterface product = null;
        CatalogProductRepositoryV1GetRequest productRequest = new CatalogProductRepositoryV1GetRequest();
        productRequest.setSku(sku);

        CatalogProductRepositoryV1Service productRepository = new CatalogProductRepositoryV1Service();
        try {
            CatalogProductRepositoryV1PortType service = productRepository.getCatalogProductRepositoryV1Port();
            addAuthHeaders(service);

            CatalogProductRepositoryV1GetResponse response = service.catalogProductRepositoryV1Get(productRequest);
            product = response.getResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return product;
    }

    private static CatalogDataProductAttributeInterface getAttribute(String attributeCode) {
        CatalogDataProductAttributeInterface attribute = null;
        CatalogProductAttributeRepositoryV1GetRequest attributeRequest = new CatalogProductAttributeRepositoryV1GetRequest();
        attributeRequest.setAttributeCode(attributeCode);

        CatalogProductAttributeRepositoryV1Service attributeRepository = new CatalogProductAttributeRepositoryV1Service();
        try {
            CatalogProductAttributeRepositoryV1PortType service = attributeRepository.getCatalogProductAttributeRepositoryV1Port();
            addAuthHeaders(service);

            CatalogProductAttributeRepositoryV1GetResponse response = service.catalogProductAttributeRepositoryV1Get(attributeRequest);
            attribute = response.getResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return attribute;
    }

    private static void addAuthHeaders(Object service) {
        Client client = ClientProxy.getClient(service);

        Map<String, List<String>> headers = new HashMap<>();
        headers.put("Authorization", Collections.singletonList("Bearer ouba1j40ol7cdbrmmcfw5bgac10k2h77"));

        client.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);
    }
}
